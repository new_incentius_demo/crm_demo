import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
    },
    state:{
      new_cust_info : [
        {
          'Custom_id' : 'C1',
          'Name' : 'Bagla Divyang',
          'address_cus': 'Karve Nagar',
          'state_cus': 'Maharashtra',
          'city_cus' : 'Pune',
          'zip_cus' : '411038'
      },
      {
          'Custom_id' : 'C2',
          'Name' : 'Curran Jeff',
          'address_cus': 'Kothrud paud road',
          'state_cus': 'Maharashtra',
          'city_cus' : 'Pune',
          'zip_cus' : '411038'
      }
      ],
    },
    mutations:{
      add_cust_data(state,new_data){
      state.new_cust_info.push(new_data)
    },
     add_whole_data(state,new_data){
      state.new_cust_info = new_data
    }
  },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
