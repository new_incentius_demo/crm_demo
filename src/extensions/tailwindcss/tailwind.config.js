module.exports = {
    theme: {
        extend: {
            width:{
                '25' : '120px',
                '26' : '130px',
                '72' : '370px',
            },
            padding:{
                '7' : '27px',
             },
             margin:{
                 '17' : '72px',
                 '13' : '62px',
                 '14' : '69px',
                 '28' : '115px'
             },
            colors:{
                'nav-b' :'#1C4E80',
            }
        },
        screens:{
            'xs': '280px',
            'sm': '640px',
            'md': '768px',
            'lg':'1024px',
            'xl':'1280px',

        },
    },
    
    prefix:  'tw-',
    
    variants: {},
    plugins: []
}
