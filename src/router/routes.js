
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/customeractivity', component: () => import('pages/CustomerActivity.vue') },
      { path: '/customeractivity/customerinformation/:ID', component: () => import('pages/CustomerInfo.vue') },
      { path: '/customeractivity/:ID', component: () => import('pages/CustomerDetailView.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
