# CRM Dashboard 

To run the dashboard on your localhost, follow the below steps :-

## Clone the Repository 
```bash
git clone git@gitlab.com:new_incentius_demo/crm_demo.git
```

## Install the dependencies
```bash
npm install
```

### Run the server 
```bash
quasar dev
```

## Data used in this dashboard :-
Tab Name        | Json File Name
--------------- | -------------
Change Request  | assets/change_request.json 

For the change request tab the above listed json file is used and the other tab i.e customer activity one it's layout is done but the data is not added.
